// write a function which takes one argument which is a number
// and returns the same value as a string

function numberToString(num) {
    return String(num);
}

console.log(numberToString(10));
console.log(typeof numberToString(10));

// returns
//"10" 