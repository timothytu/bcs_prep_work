// Write a function called swap that takes one argument, an object, and returns another object where the key/value pairs have been swapped. The original object should not be modified.

function swap(obj) {
    var newMapping = {};
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        newMapping[obj[key]] = key;
      }
    }
    return newMapping;
}

// Example:

var obj = {a: 1, b: 2}
var newObje = swap(obj) 
console.log(newObje); // {1: 'a', 2: 'b'} 