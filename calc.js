//write a function called calc that takes 3 arguments the first 2 are numbers and the third is an arithmetic operator, 
// so it is either + , -, *, /
// and it executes the appropriate operation according to the provided arithmetic operator.
// make sure you test your function with all 4 arithmetic operations

// in case the third arguments is  / or * and the second argument is not provided, the second argument should default to one.
// in case the third arguments is +  or  - and the second argument is not provided, the second argument should default to zero.

var validOperators = ['+', '-', '*', '/'];

function calc() {
    var length = arguments.length;
    var arg1, arg2, arg3;
    var num1, num2, operator;
    if (2 == length) {
        arg1 = arguments[0];
        arg2 = arguments[1];
        if (!isNaN(arg1) && validOperators.includes(arg2)) {
            num1 = arg1;
            num2 = null;
            operator = arg2;
        } else {
            console.log('invalid arguments');
        }
    } else if (3 == length) {
        arg1 = arguments[0];
        arg2 = arguments[1];
        arg3 = arguments[2];
        if (!isNaN(arg1) && !isNaN(arg2) && validOperators.includes(arg3)) {
            num1 = arg1;
            num2 = arg2;
            operator = arg3;
        } else {
            console.log('invalid arguments');
        }
    } else {
        console.log('invalid arguments');
    }
    var result;
    switch (operator) {
         case '+':
            result = (null === num2) ? num1 : num1 + num2;
          break;
          case '-':
            result = (null === num2) ? num1 : num1 - num2;
          break;
          case '*':
            result = (null === num2) ? num1 : num1 * num2;
          break;
          case '/':
            result = (null === num2) ? num1 : num1 / num2;
          break;
      }
      console.log(result);
  }

calc(10,5,'+') //15
calc(10,"/")   //10
calc (30,"*")  //30
calc (2,"+")   //2
calc (3,"-")   //3
calc(10,5,'-') //5
calc(10,5,'*') //50
calc(10,5,'/') //2