// Pseudo Latin

// Create a function which takes a string as an argument and 
// moves the first letter of each word to the end of it, then adds "ay"
// to the end of the word. Leave punctuation marks untouched.

function makeLatin(text) {
    var newText = '';
    var words = text.split(' ');
    [...words].forEach(word => {
        newWord = word.substring(1, word.length) + word.charAt(0) + 'ay';
        newText = (0 == newText.length) ? 
            newText + newWord : newText + ' ' + newWord;
    });
    return newText;
}

console.log(makeLatin("I speak latin"));

// should return:
//"Iay peaksay atinlay"

