/* 
write a function called vendingMachine
it should take 2 arguments:
– number for the amount of money entered
– number which refers to a snack you want to buy

It should return 
"You -snack- has been served", instead of a -snack- it should output 
a name of selected snack

If selected snack is not available in the vending machine it should return
"Sorry, selected snack is not available"

If selected snack is more expensive than the money amount given it should return
"Sorry, you have to insert more coins"

This is the content of our vending machine:
1. Espresso, cost: 1€
2. Cappuccino, cost: 2,50€
3. Chocolate bar, cost 2€
4. Potato chips, cost 3,50€
*/

var inventory = {
    Expresso        : 1,
    Cappuccino      : 2.5,
    'Chocolate bar' : 2,
    'Potato chips'  : 3.5
}

function vendingMachine(money, snack, items = inventory) {
    var cost = items[snack];
    if (cost == undefined) {
        console.log("Sorry, selected snack is not available");
    } else if (cost > money) {
        console.log("Sorry, you have to insert more coins");
    } else {
        console.log("Your " + snack + " has been served");
    }
}

console.log();
vendingMachine(4, 'Cappuccino');
vendingMachine(1, 'Cappuccino');
vendingMachine(1, 'coffee');
console.log();